#include "color.h"

namespace types {

Color::Color(Uint8 r, Uint8 g, Uint8 b) : r{r}, g{g}, b{b} {}
Color::Color() : r{0}, g{0}, b{0} {}

} // namespace types