#include "label.h"

namespace game {
  
Label::Label(int x, int y, int width, int height, std::string text, std::string font_name, int size, types::Color color, int style) 
  : x{x}, y{y}, width{width}, height{height}, text{text}, font_name{font_name}, size{size}, color{color}, style{style} {

  this->original_x = x;
  this->original_y = y;
}


rendering::Text Label::render() {
  return rendering::Text(this->x, this->y, 
                          this->width, this->height,
                          this->text, this->font_name, this->size,
                          this->color, this->style
    );
}

void Label::setPlayArea(PlayArea* pa, ui::Side side) {
  this->pa = pa;
  this->side = side;
}

void Label::update(rendering::Context &ctx) {
  auto play_pos = this->pa->computePos();
  auto play_size = this->pa->computeSize();
  switch (this->side)
  {
  case ui::Right:
    this->x = this->original_x + play_pos.first + play_size.first;
    break;
  
  case ui::Left:
    this->x = - this->original_x + play_pos.first - this->width;
    break;
  
  default:
    std::cerr << "Score placing error: invalid side" << std::endl;
    break;
  }
}

} // namespace game