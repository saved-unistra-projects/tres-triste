#include <vector>

#include "rendered_object.h"

namespace rendering {

template <>
RenderedObject<SDL_Rect>::RenderedObject()
    : GeneralRenderedObject(types::SDL_Rect) {}
    
template<>
RenderedObject<ColoredRectangle>::RenderedObject()
    : GeneralRenderedObject(types::ColoredRectangle) {}
    
template<>
RenderedObject<std::vector<ColoredRectangle>>::RenderedObject()
    : GeneralRenderedObject(types::ColoredRectangleList) {}

template<>  
RenderedObject<Text>::RenderedObject()
    : GeneralRenderedObject(types::Text) {}
}