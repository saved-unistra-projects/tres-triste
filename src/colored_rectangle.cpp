#include "colored_rectangle.h"

namespace rendering {
  
ColoredRectangle::ColoredRectangle(int x, int y, int width, int height, types::Color color)
    : x{x}, y{y}, width{width}, height{height}, color{color} {}
    
::types::Color ColoredRectangle::getColor() {
    return this->color;
}

SDL_Rect ColoredRectangle::toSDL_Rect() {
    return SDL_Rect { this->x, this->y, this->width, this->height };
}

} // namespace rendering