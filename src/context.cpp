#include "context.h"

namespace rendering {

Context::Context(SDL_Window *window) : window{window} {}

int Context::getWidth() {
  int w;
  SDL_GetWindowSize(this->window, &w, nullptr);
  return w;
}

int Context::getHeight() {
  int h;
  SDL_GetWindowSize(this->window, nullptr, &h);
  return h;
}

} // namespace rendering