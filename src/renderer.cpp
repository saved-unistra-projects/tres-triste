#include "renderer.h"

namespace rendering {
  
Renderer::Renderer(SDL_Surface *sprite_atlas, const char *name, int width, int height) {
  this->window =
      SDL_CreateWindow(name, SDL_WINDOWPOS_UNDEFINED,
                       SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_SHOWN);
  this->sdl_rend =
      SDL_CreateRenderer(this->window, -1, SDL_RENDERER_ACCELERATED);
  this->sprite_atlas =
      SDL_CreateTextureFromSurface(this->sdl_rend, sprite_atlas);
  SDL_SetRenderDrawBlendMode(this->sdl_rend, SDL_BLENDMODE_NONE);
}

Renderer::~Renderer() {
  SDL_DestroyTexture(this->sprite_atlas);
  SDL_DestroyRenderer(this->sdl_rend);
  SDL_DestroyWindow(this->window);
}

void Renderer::setBackground(Background &&background) {
  this->background = std::make_unique<Background>(background);
}

void Renderer::renderBackground() {
  if (!this->background)
    return;
  SDL_Rect dest = this->background->render();
  SDL_Rect srcBg = this->background->getSpritePos();
  int h, w;
  SDL_GetWindowSize(this->window, &w, &h);

  // Cover the background with a grid of background sized sprite cells
  for (int j = 0; j < h; j += this->background->getSizeY())
    for (int i = 0; i < w; i += this->background->getSizeX()) {
      dest.x = i;
      dest.y = j;
      // Render background sprite as a tile
      SDL_RenderCopy(this->sdl_rend, this->sprite_atlas, &srcBg, &dest);
    }
}

template <>
// Basic mode: render a SDL_Rect
void Renderer::renderObject(RenderedObject<SDL_Rect> &object_to_render) {
  // Classical structure: copy a rectangle of a source sprite onto the screen
  SDL_Rect dst = object_to_render.render();
  SDL_Rect sprite_pos = object_to_render.getSpritePos();
  SDL_RenderCopy(this->sdl_rend, this->sprite_atlas, &sprite_pos, &dst);
}

template <>
// Second mode: render a homemade colored rectangle
void Renderer::renderObject(
    RenderedObject<ColoredRectangle> &object_to_render) {
  
  // Convert the homemade structure into a visual SDL structure
  ColoredRectangle rec = object_to_render.render();
  
  this->renderColoredRectangle(rec);

}

void Renderer::renderColoredRectangle(ColoredRectangle cr) {
  auto sdl_rec = cr.toSDL_Rect();
  auto color = cr.getColor();
  
  // Setup the color and fill the rectangle
  SDL_SetRenderDrawColor(this->sdl_rend, color.r, color.g, color.b, 255);
  SDL_RenderFillRect(this->sdl_rend, &sdl_rec);
}

template<>
// Third mode : render a list
void Renderer::renderObject(RenderedObject<std::vector<ColoredRectangle>> &object_to_render) {
  std::vector<ColoredRectangle> list = object_to_render.render();
  for (const auto& obj : list) {
    this->renderColoredRectangle(obj);
  }
}

template<>
// Fourth mode: render a text
void Renderer::renderObject(
    RenderedObject<Text> &object_to_render) {

  Text text = object_to_render.render();
  SDL_Rect dst = text.toSDL_Rect();
  auto color = text.getColor();

  TTF_Font *font = TTF_OpenFont(text.getFontName(), text.getFontSize());
  if (!font) {
    std::cerr << "Font loading failure: " << text.getFontName() << " " << text.getFontSize() << std::endl;
  }

  TTF_SetFontStyle(font, text.getStyle());
  SDL_Surface* surface = TTF_RenderUTF8_Shaded(font, text.getText(), {color.r, color.g, color.b}, {0,0,0});

  auto texture = SDL_CreateTextureFromSurface(this->sdl_rend, surface);
  SDL_RenderCopy(this->sdl_rend, texture, NULL, &dst);
  
  SDL_DestroyTexture(texture);
  SDL_FreeSurface(surface);
  TTF_CloseFont(font);
}

void Renderer::updateWindow() { SDL_RenderPresent(this->sdl_rend); }

std::pair<int, int> Renderer::getWindowSize() {
  int w, h;
  SDL_GetWindowSize(this->getWindow(), &w, &h);
  return std::make_pair(w, h);
}

void Renderer::clearWindow(int width, int height) {
  this->renderColoredRectangle(ColoredRectangle{0, 0, width, height, ::types::Color(0, 0, 0)});
}

} // namespace rendering