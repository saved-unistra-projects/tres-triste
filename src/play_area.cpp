#include "play_area.h"
#include "globals.h"


namespace game {
rendering::ColoredRectangle PlayArea::render() {
  auto pos = this->computePos();
  auto size = this->computeSize();
  
  return rendering::ColoredRectangle(pos.first, pos.second, size.first,
                                     size.second, this->color);
}

std::pair<int, int> PlayArea::computePos() {
  auto window_size = globals::main_loop->getRenderer().getWindowSize();
  auto size = this->computeSize();

  if (this->pos_defined) {
    if (this->anchor == Right) {
      return std::make_pair(
          (window_size.first - this->pos.first) - size.first,
          this->pos.second);
    } else if (this->anchor == Left) {
      return this->pos;
    }
  }

  int x = ((window_size.first - size.first) / 2);
  int y = 0;
  return std::make_pair(x, y);
}

std::pair<int, int> PlayArea::computeSize() {
  if (this->size_defined) {
    return this->size;
  }
  double ratio = static_cast<double>(this->multiple_x) /
                 static_cast<double>(this->multiple_y);
  auto window_size = globals::main_loop->getRenderer().getWindowSize();
  int width = window_size.second * ratio;
  int height = window_size.second;
  width -= (width % this->multiple_x);
  height -= (height % this->multiple_y);

  return std::make_pair(width, height);
}

void DoublePlayArea::init(rendering::Context &ctx) {
  this->pa1->setPos(0, 0);
  this->pa2->setAnchor(PlayArea::Right);
  this->pa2->setPos(0, 0);
}

void DoublePlayArea::update(rendering::Context &ctx) {
}
} // namespace game