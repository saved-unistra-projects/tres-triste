#include <SDL.h>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "globals.h"
#include "tetris_image.h"


namespace game {

TetrisImage::TetrisImage(int x, int y, TetrisShapePtr shape, game::PlayArea &pa, ui::Side side)
    : x{x}, y{y}, pa{pa}, shape{shape} {

  // Set the dimensions for the background cover
  this->width = (WIDEST_WIDTH)*TetrisBlock::width;
  this->height = (HEIGHEST_HEIGHT)*TetrisBlock::height;

  // Change pos depending on the chosen side
  auto play_pos = pa.computePos();
  auto play_size = pa.computeSize();

  switch (side)
  {
  case ui::Right:
    this->x = this->x + play_pos.first + play_size.first;
    break;
  
  case ui::Left:
    this->x = - this->x + play_pos.first - this->width;
    break;
  
  default:
    std::cerr << "Label placing error: invalid side" << std::endl;
    break;
  }
}

void TetrisImage::registerBlocks() {
  auto play_pos = this->pa.computePos();
  auto play_size = this->pa.computeSize();
  int percx = play_size.first / BLOCKS_WIDE;
  int percy = play_size.second / BLOCKS_HIGH;

  int block_x, block_y;
  int min_x = 0;
  int min_y = 0;

  // Compute offset of the pieces depending on its shape
  for (const auto &coord : this->shape->getShape()) {
    if (coord.first<min_x)
      min_x = coord.first;
    if (coord.second<min_y)
      min_y = coord.second;
  }

  // Convert grid coordinate to real coordinate
  int to_real_x = (play_size.first/BLOCKS_WIDE);
  int to_real_y = (play_size.second/BLOCKS_HIGH);

  // Place the blocks and add them in the rendering loop
  for (const auto &coord : this->shape->getShape()) {
    block_x = coord.first-min_x+(this->x-play_pos.first)/to_real_x;
    block_y = coord.second-min_y+(this->y-play_pos.second)/to_real_y;
    
    TetrisBlock *block = new TetrisBlock(
      block_x, block_y, 
      pa, this->shape->getColor()
    );
    block->setGhost(true);
    this->blocks.push_back(block);
  }
}

std::vector<rendering::ColoredRectangle> TetrisImage::render() {
  auto play_pos = this->pa.computePos();
  auto play_size = this->pa.computeSize();
  int percx = play_size.first / BLOCKS_WIDE;
  int percy = play_size.second / BLOCKS_HIGH;

  // Background color
  /*std::vector<rendering::ColoredRectangle> image = {
    rendering::ColoredRectangle(this->x, this->y,
                                this->width, this->height, 
                                types::Color(0, 0, 0))
  };*/
  std::vector<rendering::ColoredRectangle> image;

  // Add the rendered version of each block in the returned vector
  for (const auto &block : this->blocks) {
    std::vector<rendering::ColoredRectangle> rendered_block = block->render();
    image.insert(image.end(), rendered_block.begin(), rendered_block.end());
  }

  return image;
}

} // namespace game