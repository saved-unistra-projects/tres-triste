#include <SDL.h>
#include <iostream>
#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <random>
#include <time.h>
#include <unistd.h>
#include <getopt.h>

#include "collision_object.h"
#include "renderer.h"
#include "background.h"
#include "context.h"
#include "main_loop.h"
#include "tetris_block.h"
#include "tetris_piece.h"
#include "debug.h"
#include "globals.h"
#include "play_area.h"
#include "tetris.h"
#include "label.h"
#include "options.h"

engine::MainLoop *globals::main_loop;

int main(int argc, char** argv) {
	int players = 1, c, opt_index, i;
	bool bot = false;
	while ((c = getopt_long(argc, argv, "p:b", long_options, &opt_index)) != -1) {
		switch(c) {
			case 'p':
				players = atoi(optarg);
				if (players < 1 || players > 2) {
					std::cerr << "Invalid number of players" << std::endl;
					exit(EXIT_FAILURE);
				}
				break;
			case 'b':
				bot = true;
				break;
			default:
				break;
		}
	}
	

	try {
		globals::main_loop = new engine::MainLoop();
	} catch (const std::string e) {
		std::cerr << e << std::endl;
		exit(EXIT_FAILURE);
	}

	// One player case
	
	// Set a random seed...
	srand(time(nullptr));
	/*...And generate a random seed for the Tetris games so that it's random b
	but the same for both players */
	int seed = rand();
	
	if (players == 1) {
		// Create a gray play area of 10 by 20
		auto pa = new game::PlayArea(10, 20, types::Color(70, 70, 70));
		// Create the tetris object that will handle the game
		auto tetris = new game::Tetris("pieces", pa, 10, 20);
		// Set the seed
		tetris->setSeed(seed);
		// Register both the play area and the tetris
		globals::main_loop->registerObject(pa);
		globals::main_loop->registerObject(tetris);
	}
	// 2 players case
	else if (players == 2) {
		
		// Create 2 play areas
		auto pa1 = new game::PlayArea(10, 20, types::Color(70, 70, 70));
		auto pa2 = new game::PlayArea(10, 20, types::Color(70, 70, 70));
		// Those play areas will be handled by this object
		auto dpa = new game::DoublePlayArea(pa1, pa2);
		
		// Create the Tetris games with a different area for each
		auto tetris1 = new game::Tetris("pieces", pa1, 10, 20);
		auto tetris2 = new game::Tetris("pieces", pa2, 10, 20);
		
		// For one of the Tetris games, change the control scheme :
		tetris1->setPieceControls(game::TetrisPiece::PieceControls(
			SDL_SCANCODE_S,
			SDL_SCANCODE_A,
			SDL_SCANCODE_D,
			SDL_SCANCODE_W,
			SDL_SCANCODE_SPACE
		));
		
		// Also change the side of the UI for the first Tetris
		tetris1->setInterfaceSide(ui::Right);
		// If AI, indicate it
		if (bot)
			tetris1->setAIControlled(true);
		
		/* Change the collision layer for one of the players (so that
		the pieces from each game don't collide)*/
		tetris2->setLayer(1);
		
		// Set the same seed for both
		tetris1->setSeed(seed);
		tetris2->setSeed(seed);
		
		// Register everything
		globals::main_loop->registerObject(dpa);
		globals::main_loop->registerObject(pa1);
		globals::main_loop->registerObject(pa2);
		globals::main_loop->registerObject(tetris1);
		globals::main_loop->registerObject(tetris2);
	}
	
	// Start everything !
	globals::main_loop->start();
	return 0;
}

