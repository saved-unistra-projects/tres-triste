#include <iostream>
#include <fstream>

#include "tetris_shape.h"

namespace game {
TetrisShape::TetrisShape(Shape shape, size_t weight, types::Color color) : weight{weight}, shape{shape}, color{color} {
}

TetrisShape::TetrisShape(const char *filename) {
  std::ifstream file;
  file.open(filename);

  if (!file) {
    std::cerr << "Error : could not open file " << filename << std::endl;
  }
  
  // Parse weight
  file >> this->weight;
  int r, g, b;
  // Parse color
  file >> r >> g >> b;
  this->color = types::Color(r, g, b);

  // Parse number of blocks
  int n;
  file >> n;
  
  this->shape = Shape();

  for (int i = 0; i < n; i++) {
    int x, y;
    file >> x >> y;
    this->shape.push_back(std::make_pair(x, y));
  }
  
  if (!file.eof()) {
    std::string rot;
    file >> rot;
    if (rot == "no_rotation")
      this->rotation = false;
    if (rot == "limited_rotation")
      this->limited_rotation = true;
  } 
  file.close();
}
} // namespace game