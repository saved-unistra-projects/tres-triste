#include "background.h"

namespace rendering {

Background::Background(SDL_Rect sprite_pos, int size_x, int size_y)
: size_x{size_x}, size_y{size_y} 
{
  this->sprite_pos = sprite_pos;
}

SDL_Rect Background::render() 
{
  return SDL_Rect {0, 0, this->sprite_pos.w, this->sprite_pos.h};
}

}