#include "text.h"

namespace rendering {

Text::Text(int x, int y, int width, int height, std::string text, std::string font_name, int size, types::Color color, int style)
  : x{x}, y{y}, width{width}, height{height}, text{text}, size{size}, color{color}, style{style} {

  this->font_name = FONT_REP + font_name;
}

::types::Color Text::getColor() {
  return this->color;
}

const char* Text::getText() {
  return this->text.c_str();
}

const char* Text::getFontName() {
  return this->font_name.c_str();
}

int Text::getFontSize() {
  return this->size;
}

int Text::getStyle() {
  return this->style;
}

SDL_Rect Text::toSDL_Rect() {
  return SDL_Rect { this->x, this->y, this->width, this->height };
}

}