#include "score_label.h"

namespace ui {

void ScoreLabel::setScore(unsigned int score) {
  this->score = score;
  this->text = "Score: " + std::to_string(this->score);
}

void ScoreLabel::addToScore(unsigned int points) {
  this->score += points;
  this->text = "Score: " + std::to_string(this->score);
}

}