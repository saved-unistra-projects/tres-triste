#include "tetris_block.h"

namespace game {
TetrisBlock::TetrisBlock(int x, int y, game::PlayArea &pa, types::Color color,
                         int layer)
    : CollisionObject{x, y, layer}, color{color}, pa{pa} {
  this->x = x;
  this->y = y;
}

std::vector<rendering::ColoredRectangle> TetrisBlock::render() {
  auto play_pos = this->pa.computePos();
  auto play_size = this->pa.computeSize();
  int percx = play_size.first / BLOCKS_WIDE;
  int percy = play_size.second / BLOCKS_HIGH;

  int pos_x = this->x * percx + play_pos.first;
  int pos_y = this->y * percy + play_pos.second;

  int light_width = static_cast<float>(percx) * DEPTH_MULT;
  int light_height = static_cast<float>(percy) * DEPTH_MULT;

  int light_pos_x = pos_x + (percx - light_width) / 2.;
  int light_pos_y = pos_y + (percy - light_height) / 2.;

  auto cap = [](int value, int cap) { return value < cap ? value : cap; };

  types::Color light_color =
      types::Color(cap(this->color.r + LIGHT_COLOR_DIFF, 255),
                   cap(this->color.g + LIGHT_COLOR_DIFF, 255),
                   cap(this->color.b + LIGHT_COLOR_DIFF, 255));

  return std::vector<rendering::ColoredRectangle>{
      rendering::ColoredRectangle(this->x * percx + play_pos.first,
                                  this->y * percy + play_pos.second, percx,
                                  percy, this->color),
      rendering::ColoredRectangle(light_pos_x, light_pos_y, light_width,
                                  light_height, light_color)};
}
void TetrisBlock::incrementY() { ++this->y; }

void TetrisBlock::incrementX() { ++this->x; }

void TetrisBlock::decrementX() { --this->x; }

} // namespace game