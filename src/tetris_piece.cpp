#include <SDL.h>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "globals.h"
#include "tetris_piece.h"

namespace game {
TetrisPiece::TetrisPiece(int x, int y, TetrisShapePtr shape, game::PlayArea &pa,
                         int speed, int layer, PieceControls pc)
    : x{x}, y{y}, pa{pa}, shape{shape}, pc{pc}, layer{layer} {
  this->time_until_drop = 1. / static_cast<double>(speed);
  this->registerBlocks();
  this->correctStartingPos();
  this->rotation = shape->getRotation();
  this->limited_rotation = shape->getLimitedRotation();
}

void TetrisPiece::update(rendering::Context &ctx) {
  if (this->is_alive) {
    this->time_passed += ctx.getDelta();
    this->checkCollisions();
    if (this->time_passed > this->time_until_drop) {
      // If there is something under when I try to go down => destruct
      if (this->collision_mask & physics::CollisionObject::Down) {
        this->destruct();
        // Return so it doesn't move down
        return;
      }
      this->time_passed = 0.;
      this->move(Down);
    }
    this->moved_this_frame = false;
  }
}

void TetrisPiece::onKeyPressed(const Uint8 *keys, rendering::Context &ctx) {
  // Start by computing collision
  this->checkCollisions();
  /* Just check if the acceleration key went back up
    Not that beautiful but simple enough. Not too much voodoo for our purposes
  */
  
  if (this->accelerating) {
    if (!keys[this->pc.down]) {
      this->accelerate(false);
    }
  }
  
  // Increment counter for key repeat

  /* This bit of code prevents the controls to be spammed :
     when a (meaningful) key is pressed, we remember which one it is,
     then every frame, we check if it went back up, if it didn't, we simply
     return as to not allow the player to input any other control. */
     
  if (this->key_to_watch != -1) {
    this->key_timer_counter += ctx.getDelta();
    if (!keys[this->key_to_watch] || this->key_repeat_timer < this->key_timer_counter) {
      this->key_to_watch = -1;
      this->key_timer_counter = 0.;
    }
  }
  if (this->rotation_used) {
    this->rotation_counter += ctx.getDelta();
    if (!keys[this->pc.rotate] || this->key_repeat_timer < this->rotation_counter) {
      this->rotation_used = false;
      this->rotation_counter = 0.;
    }
  }
  if (this->key_to_watch != this->pc.left && keys[this->pc.left] &&
      !(this->collision_mask & physics::CollisionObject::Left)) {
    this->move(Left);
    this->key_to_watch = this->pc.left;
  }
  if (this->key_to_watch != this->pc.right && keys[this->pc.right] &&
      !(this->collision_mask & physics::CollisionObject::Right)) {
    this->move(Right);
    this->key_to_watch = this->pc.right;
  }
  if (!this->rotation_used && keys[this->pc.rotate]) {
    this->move(Rotate);
    this->rotation_used = true;
  }
  if (keys[this->pc.down] && !this->accelerating) {
    this->accelerate(true);
  }
}

void TetrisPiece::registerBlocks() {
  for (const auto &coord : this->shape->getShape()) {
    TetrisBlock *block = new TetrisBlock(coord.first + this->x, coord.second + this->y, pa, this->shape->getColor(), this->layer);
    this->blocks.push_back(block);
    globals::main_loop->registerObject(block);
    /* Ghost all blocks so that they don't "collide" with each other */
    block->setGhost(true);
  }
}

void TetrisPiece::destroyBlocks() {
  for (auto &block : this->blocks) {
    block->markDelete();
  }
}

void TetrisPiece::pressKey(Uint8 scancode, rendering::Context &ctx) {
  int num_keys;
  SDL_GetKeyboardState(&num_keys);
  // Create a fake key array
  Uint8 *keys = (Uint8*) calloc(sizeof(Uint8), num_keys);
  keys[scancode] = 1;
  this->onKeyPressed(keys, ctx);
  free(keys);
}

void TetrisPiece::move(Move m) {
   if (this->moved_this_frame)
     return;
  this->moved_this_frame = true;
  switch (m) {
  case Down:
    this->y += 1;
    for (const auto &block : this->blocks) {
      block->incrementY();
    }
    break;
  case Left:
    this->x -= 1;
    for (const auto &block : this->blocks) {
      block->decrementX();
    }
    break;
  case Right:
    this->x += 1;
    for (const auto &block : this->blocks) {
      block->incrementX();
    }
    break;
  case Rotate:
    if (this->rotation) {
      if (this->limited_rotation) {
        if (this->rotated) {
          this->rotate(Clockwise);
          this->rotated = false;
        }
        else {
          this->rotate(AntiClockwise);
          this->rotated = true;
        }
      }
      else {
        this->rotate(AntiClockwise);
      }
    }
    break;
  }
}

void TetrisPiece::accelerate(bool b) {
  if (b) {
    this->old_time_until_drop = this->time_until_drop;
    this->time_until_drop = FAST_SPEED;
  } else {
    this->time_until_drop = this->old_time_until_drop;
  }
  this->accelerating = b;
}

void TetrisPiece::checkCollisions() {
  this->collision_mask = 0;
  for (const auto &block : this->blocks) {
    this->collision_mask |= block->getCollisionMask();
  }
}

void TetrisPiece::rotate(RotationDirection rd) {
  std::vector<std::pair<int, int>> old_coords;
  for (const auto &block : this->blocks) {
    old_coords.push_back(std::make_pair(block->getX(), block->getY()));
    int x = block->getX() - this->x;
    int y = block->getY() - this->y;
    if (rd == AntiClockwise) {
      block->setX(y + this->x);
      block->setY(-x + this->y);
    }
    else {
      block->setX(- y + this->x);
      block->setY(x + this->y);
    }
  }
  bool revert_coords = false;
  for (const auto &block : this->blocks) {
    block->poll();
    if (block->getCollisionMask() & physics::CollisionObject::On)  {
      revert_coords = true;
      break;
    }
  }
  if (revert_coords) {
    size_t count = 0;
    for (auto &block : this->blocks) {
      block->setX(old_coords[count].first);
      block->setY(old_coords[count].second);
      ++count;
    }
  }
}

void TetrisPiece::destruct() {
  this->is_alive = false;
  for (const auto &block : this->blocks) {
    // Unghost all blocks
    block->setGhost(false);
  }
}

void TetrisPiece::correctStartingPos() {
  int min_coord = 0;
  // Find the highest piece
  for (const auto &block : this->shape->getShape()) {
    if (block.second < min_coord) {
      min_coord = block.second;
    }
  }
  // Move down for each blocks above the center.
  for (int i = 0; i > min_coord; --i) {
    move(Down);
  }
}
} // namespace game