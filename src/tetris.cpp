#include <algorithm>
#include <dirent.h>
#include <map>
#include <random>
#include <time.h>
#include <thread>

#include "tetris.h"

namespace game {

Tetris::Tetris(std::vector<TetrisShapePtr> piece_pool, PlayArea *pa, int size_x,
               int size_y)
    : size_x{size_x}, size_y{size_y}, pa{pa} {
  // Sort piece pool and compute weight sum for random selection of pieces
  this->piece_pool = piece_pool;
}

Tetris::~Tetris() {
  delete this->next_piece;
}

Tetris::Tetris(const char *directory, PlayArea *pa, int size_x, int size_y)
    : size_x{size_x}, size_y{size_y}, pa{pa} {
  this->piece_pool = std::vector<TetrisShapePtr>();

  /* Loop through directory, parsing every file. */
  struct dirent *entry = nullptr;
  DIR *dp = nullptr;

  dp = opendir(directory);
  if (dp != nullptr) {
    while ((entry = readdir(dp))) {
      std::string filename = entry->d_name;
      if (filename.front() != '.') {
        std::string full_filename =
            std::string(directory) + std::string("/") + filename;
        this->piece_pool.push_back(
            std::make_shared<TetrisShape>(TetrisShape(full_filename.c_str())));
      }
    }
  }
  closedir(dp);
  initTetris();
}

void Tetris::initTetris() {
  std::sort(this->piece_pool.begin(), this->piece_pool.end(),
            [](TetrisShapePtr a, TetrisShapePtr b) {
              return a->getWeight() > b->getWeight();
            });
  this->weight_sum = this->computeWeightSum();

  // Create the play area
  // this->pa = new PlayArea(size_x, size_y, types::Color(70, 70, 70));

  this->next_piece = new TetrisImage(0, 0, this->selectRandomPiece(), *this->pa, this->side);
  
  // Set border collisions according to grid size
  physics::CollisionObject::setBorders(0, 0, size_x - 1, size_y - 1);
  
  // Set hold key
  this->hold_key = this->piece_controls.reserve;
}

TetrisPiece &Tetris::parsePieceFile(const char *filename) {}

size_t Tetris::computeWeightSum() {
  // Loop through shapes, adding up their weights
  size_t weight_sum = 0;
  for (const auto &tp : this->piece_pool) {
    weight_sum += tp->getWeight();
  }
  return weight_sum;
}

void Tetris::addBlocks(TetrisPiece *tp) {
  for (auto const &block : tp->getBlocks()) {
    this->score->addToScore(1);
    this->blocks.push_back(block);
  }
}

void Tetris::eliminateLines() {
  // Number of deleted lines
  int deleted_lines = 0;
  // Highest deleted line
  int highest_line = this->size_y;
  // Used to keep track of indices
  std::map<int, int> blocks_per_line;
  // Initialize map to 0s
  for (int i = 0; i < this->size_y; ++i) {
    blocks_per_line[i] = 0;
  }

  // Loop through each block
  for (auto const &block : this->blocks) {
    if (block)
      ++blocks_per_line[block->getY()];
  }

  for (int i = this->size_y; i >= 0; --i) {
    // A line is deleted if there are size_x blocks in the line
    if (blocks_per_line[i] == this->size_x) {
      ++deleted_lines;
      highest_line = i;
      this->score->addToScore(this->size_x * 10);
    }
  }

  // Just quit the function if there a no deleted lines
  if (deleted_lines == 0) {
    return;
  }
  /* Loop one last time through the blocks, moving down by <deleted_lines>
   blocks the blocks above the highest deleted line, and marking the
   removed blocks for deletion */

  // Used to keep track of indices

  std::vector<int> go_down_map = std::vector<int>(size_y, -1);
  int count = 0;
  for (auto &block : this->blocks) {
    // Case where the block is to be deleted
    if (block) {
      if (blocks_per_line[block->getY()] == size_x) {
        // Remove the block from the collision grid
        block->unregister();
        block->markDelete();
        // This creates a vector with holes
        // TODO: Make this better by making new blocks fill the holes
        this->blocks[count] = nullptr;
      } else if (go_down_map[block->getY()] == -1) {
        int lines_to_go_down = deleted_lines;
        for (int i = 0; i < block->getY(); ++i) {
          if (blocks_per_line[i] == this->size_x) {
            --lines_to_go_down;
          }
        }
        go_down_map[block->getY()] = lines_to_go_down;
      }
    }
    ++count;
  }

  for (auto &block : this->blocks) {
    // Case where the block is above the highest line
    if (block && !block->toBeDeleted() && go_down_map[block->getY()] != -1) {
      block->setY(block->getY() + go_down_map[block->getY()]);
    }
  }
}

Tetris::TetrisShapePtr Tetris::selectRandomPiece() {
  /* Weighted random selection algorithm : take
   a random number between 0 and the weight sum,
   loop through shapes, substracting their weight to
   the random number. The shape the loop falls on when
   the random number gets to 0 is the chosen shape. */
  std::uniform_int_distribution<> distrib(0, this->weight_sum);
  int random_number = distrib(this->generator);
  for (const auto &tp : this->piece_pool) {
    random_number -= tp->getWeight();
    if (random_number < 0)
      return tp;
  }
  return this->piece_pool.back();
}

void Tetris::update(rendering::Context &ctx) {
  if (!this->current_piece) {
    this->current_piece = new TetrisPiece(
        this->size_x / 2, 0, this->next_piece->getShape(), *this->pa, this->speed,
        this->layer, this->piece_controls);
    this->current_shape = this->next_piece->getShape();
    if (this->ai_controlled) {
      this->current_piece->disableKeyPress();
    }
    this->current_shape = this->selectRandomPiece();
    globals::main_loop->registerObject(this->current_piece);

    if (this->next_piece != nullptr) {
      this->next_piece->markDelete();
      this->next_piece = nullptr;
    }
    this->next_piece = new TetrisImage(30, 150, this->selectRandomPiece(), *this->pa, this->side);
    this->next_piece->registerBlocks();
    globals::main_loop->registerObject(this->next_piece);
  }
  else if (!this->current_piece->isAlive()) {
    // If the pieces collide, it means the game is finished
    if (this->current_piece->getCollisionMask() & physics::CollisionObject::On) 
    {
      // Sleep a bit, then end the main loop
      this->current_piece->markDelete();
      std::this_thread::sleep_for(std::chrono::milliseconds(2000));
      this->markDelete();
      globals::main_loop->stop();
    }
    this->addBlocks(this->current_piece);
    this->current_piece->markDelete();
    this->current_piece = nullptr;
    this->eliminateLines();
    this->used_hold = false;
  }
  else if(this->ai_controlled) {
    this->bot_time += ctx.getDelta();
    if (this->bot_time < this->bot_threshold) {
      if (this->key_to_press != -1 &&
          this->key_to_press != this->piece_controls.rotate)
        this->current_piece->pressKey(this->key_to_press, ctx);
      return;
    }
    this->bot_time = 0;
    int random_key = rand() % 10;
    switch (random_key) {
      case 0:
        this->key_to_press = this->piece_controls.down;
        break;
      case 1:
        this->key_to_press = this->piece_controls.left;
        break;
      case 2:
        this->key_to_press = this->piece_controls.right;
        break;
      case 3:
        this->key_to_press = this->piece_controls.rotate;
        break;
      case 4:
        this->key_to_press = this->piece_controls.reserve;
      default:
        this->key_to_press = -1;
    }
    if (this->key_to_press != -1)  
      this->current_piece->pressKey(this->key_to_press, ctx);
  }
}

void Tetris::onKeyPressed(const Uint8 *keys, rendering::Context &ctx) {
  // Handle the hold
  if (keys[this->hold_key] && !this->used_hold) {
    if (this->held_piece == nullptr) {
      this->held_piece = this->current_shape;

      this->held_image = new TetrisImage(30, 370, this->current_shape, *this->pa, this->side);
      this->held_image->registerBlocks();
      globals::main_loop->registerObject(this->held_image);

      this->current_piece->markDelete();
      // Completely destroy the blocks of the piece
      this->current_piece->destroyBlocks();
      /* Setting this to null will make the update function create a new random
      one */
      this->current_piece = nullptr;

    } else {
      // Swap the shapes
      auto tmp = this->held_piece;
      this->held_piece = this->current_shape;

      this->held_image->markDelete();
      this->held_image = new TetrisImage(30, 370, this->current_shape, *this->pa, this->side);
      this->held_image->registerBlocks();
      globals::main_loop->registerObject(this->held_image);

      this->current_shape = tmp;
      /* Completely destroy the current piece */
      this->current_piece->markDelete();
      this->current_piece->destroyBlocks();
      // Replace the current piece with the held shape
      // TODO : Redondant code, needs to be refactored
      this->current_piece =
          new TetrisPiece(this->size_x / 2, 0, this->current_shape, *this->pa,
                          this->speed, this->layer, this->piece_controls);
      globals::main_loop->registerObject(this->current_piece);
    }
    this->used_hold = true;
  }
}

void Tetris::init(rendering::Context &ctx) {
  // Start by putting the play area in place
  globals::main_loop->registerObject(this->pa);

  // Create the score interface and place it
  this->score =
      new ui::ScoreLabel(10, 5, 80, 30, "Score: 0", "Ubuntu-C.ttf", 20,
                         types::Color(255, 255, 255), BOLD | UNDERLINE);
  this->score->setPlayArea(this->pa, this->side);
  globals::main_loop->registerObject(this->score);
  
  this->preview_label = new game::Label(10, 100, 60, 30, "Next:", "Ubuntu-C.ttf", 20, types::Color(255, 255, 255), BOLD | UNDERLINE);
  this->preview_label->setPlayArea(this->pa, this->side);
  globals::main_loop->registerObject(this->preview_label);

  this->held_label = new game::Label(10, 300, 60, 30, "Hold:", "Ubuntu-C.ttf", 20, types::Color(255, 255, 255), BOLD | UNDERLINE);
  this->held_label->setPlayArea(this->pa, this->side);
  globals::main_loop->registerObject(this->held_label);
}
} // namespace game