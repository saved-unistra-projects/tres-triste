#include <iostream>

#include "collision_object.h"
#include "debug.h"

namespace physics {
std::shared_ptr<std::vector<CollisionObject *>>
    CollisionObject::collision_grid = nullptr;

/* Initialize static member variables */
int CollisionObject::last_id = 0;
int CollisionObject::min_x = -1;
int CollisionObject::min_y = -1;
int CollisionObject::max_x = -1;
int CollisionObject::max_y = -1;

CollisionObject::CollisionObject(int x, int y, int layer) : layer{layer} {
  if (this->collision_grid != nullptr) {
  } else {
    CollisionObject::collision_grid =
        std::shared_ptr<std::vector<CollisionObject *>>(
            new std::vector<CollisionObject *>);
  }
  this->id = this->last_id;
  this->last_id++;
  // TODO: Change later
  this->position_in_grid = id;
  this->collision_grid.get()->push_back(this);
}

CollisionObject::~CollisionObject() {
  (*this->collision_grid.get())[this->position_in_grid] = nullptr;
}

void CollisionObject::poll() {
  this->collision_mask = 0;

  // Check all the collision objects
  this->checkBorders();
  for (CollisionObject *o : *this->collision_grid.get()) {
    if (!o)
      continue;
    if (!o->isGhost() && this->id != o->getId() &&
        this->layer == o->getLayer()) {
      int diffx = this->x - o->x;
      int diffy = this->y - o->y;
      if (diffx == -1 && diffy == 0) {
        this->collision_mask |= Right;
      }
      if (diffx == 1 && diffy == 0) {
        this->collision_mask |= Left;
      }
      if (diffy == -1 && diffx == 0) {
        this->collision_mask |= Down;
      }
      if (diffy == 1 && diffx == 0) {
        this->collision_mask |= Up;
      }
      if (diffy == 0 && diffx == 0) {
        this->collision_mask |= On;
      }
    }
  }
}

int CollisionObject::getCollisionMask() { return this->collision_mask; }

int CollisionObject::getId() { return this->id; }

void CollisionObject::setGhost(bool is_ghost) { this->is_ghost = is_ghost; }

bool CollisionObject::isGhost() { return this->is_ghost; }

void CollisionObject::setBorders(int min_x, int min_y, int max_x, int max_y) {
  CollisionObject::min_x = min_x;
  CollisionObject::min_y = min_y;
  CollisionObject::max_x = max_x;
  CollisionObject::max_y = max_y;
}

void CollisionObject::unregister() {
  (*this->collision_grid.get())[this->position_in_grid] = nullptr;
}

void CollisionObject::checkBorders() {
  /* If either the max or min is undefined, don't compute
     collisions */
  if (this->min_x >= 0 && this->max_x >= 0) {
    // Simply change the mask according to borders
    if (this->min_x == this->x)
      this->collision_mask |= Left;
    if (this->x < this->min_x)
      this->collision_mask |= On;
    if (this->max_x == this->x)
      this->collision_mask |= Right;
    if (this->x > max_x)
      this->collision_mask |= On;
  }
  // Same thing for y
  if (this->min_y >= 0 && this->max_y >= 0) {
    if (this->min_y == this->y)
      this->collision_mask |= Up;
    if (this->y < this->min_y)
      this->collision_mask |= On;
    if (this->max_y == this->y)
      this->collision_mask |= Down;
    if (this->y > this->max_y)
      this->collision_mask |= On;
  }
}
} // namespace physics