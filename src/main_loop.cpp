#include "main_loop.h"

namespace engine {

MainLoop::MainLoop() {
  // SDL requirement
  if (SDL_Init(SDL_INIT_VIDEO) != 0) {
    throw "SDL_Init error (SDL_INIT_VIDEO != 0)";
  }
  TTF_Init();

  // Load the sprite sheet
  auto sprite_atlas = SDL_LoadBMP("./sprites.bmp");
  SDL_SetColorKey(sprite_atlas, true, 0);

  // Initialize the renderer and the window
  this->renderer =
      new rendering::Renderer(sprite_atlas, "Très triste", 1200, 600);
  this->ctx = new rendering::Context(this->renderer->getWindow());
}

MainLoop::~MainLoop() {
  // Proper removal of the displaying structures
  delete this->renderer;
  delete this->ctx;
}

void MainLoop::setBackground(rendering::Background &&background) {
  this->renderer->setBackground(std::move(background));
}

void MainLoop::start() {
  this->has_started = true;
  this->now = SDL_GetPerformanceCounter();
  this->init();
  this->loop();
}

void MainLoop::loop() {
  this->quit = false;

  while (!this->quit) {
    // Frame counter at the beginning of the iteration
    Uint64 frame_start = SDL_GetPerformanceCounter();

    // While no event is triggered
    SDL_Event event;
    while (!this->quit && SDL_PollEvent(&event)) {
      // Capture the event
      switch (event.type) {
      case SDL_QUIT:
        quit = true;
        break;

      case SDL_MOUSEBUTTONDOWN:
        std::cout << "mouse click " << event.button.button << std::endl;
        break;

      default:
        break;
      }
    }
    // An event has been captured (congrats)
    const Uint8 *state = SDL_GetKeyboardState(nullptr);
    this->physics();
    this->handleInputs(state);
    this->quit |= (state[SDL_SCANCODE_ESCAPE] != 0);

    // Compute the delta (seconds/frame since last loop)
    this->prev = this->now;
    this->now = SDL_GetPerformanceCounter();
    this->ctx->setDelta(static_cast<double>(this->now - this->prev) /
                        static_cast<double>(SDL_GetPerformanceFrequency()));

    // Update and render the list of specified entities
    this->update();
    this->render();
    this->delete_loop();

    // Frame counter at the end of the iteration
    Uint64 frame_end = SDL_GetPerformanceCounter();

    // Handle the frame per second upper limit by delaying the next iteration
    float elapsed = (frame_end - frame_start) /
                    static_cast<float>(SDL_GetPerformanceFrequency() * 1000.f);
    SDL_Delay(1000.f / this->fps_cap - elapsed);
  }

  // Proper closing of the app when a quit event is caught
  this->delete_loop();
  TTF_Quit();
  SDL_Quit();
}

void MainLoop::init() {
  /* Make a copy in case new objects get added during the initialization of
     other objects */
  std::vector<rendering::GeneralRenderedObject *> copy_vec =
      std::vector<rendering::GeneralRenderedObject *>(this->rendered_entities);
  for (rendering::GeneralRenderedObject *o : copy_vec) {
    if (o)
      o->init(*this->ctx);
  }
}

void MainLoop::registerObject(GeneralObject *obj) {
  // Add to the vector of entities to render
  auto render = dynamic_cast<rendering::GeneralRenderedObject *>(obj);
  if (render) {

    this->rendered_entities.push_back(render);
    if (this->has_started)
      render->init(*this->ctx);
  }

  // Add to the vector of entities to compute physics on
  auto physics = dynamic_cast<physics::PhysicsObject *>(obj);
  if (physics) {
    // If the object is rendered as well, we keep that in memory
    if (render)
      physics->setIsRendered(true);
    this->physics_entities.push_back(physics);
  }
}

void MainLoop::render() {
  // Before rendering anything, clear the background
  auto width = this->ctx->getWidth();
  auto height = this->ctx->getHeight();
  this->renderer->clearWindow(width, height);
  // Render the background
  this->renderer->renderBackground();

  // Render the specified entities
  for (rendering::GeneralRenderedObject *o : this->rendered_entities) {
    if (o)
      switch (o->type) {
      // Classical SDL visual structure
      case rendering::types::SDL_Rect:
        this->renderer->renderObject<SDL_Rect>(
            *dynamic_cast<rendering::RenderedObject<SDL_Rect> *>(o));
        break;

      // Homemade nice lil rectangle
      case rendering::types::ColoredRectangle:
        this->renderer->renderObject<rendering::ColoredRectangle>(
            *dynamic_cast<
                rendering::RenderedObject<rendering::ColoredRectangle> *>(o));
        break;

      /* A general rendered object is not supposed to be rendered, so it is
        skipped */
      case rendering::types::GeneralRenderedObject:
        break;

      /* List of rendering types */
      case rendering::types::ColoredRectangleList:
        this->renderer->renderObject<std::vector<rendering::ColoredRectangle>>(
            *dynamic_cast<rendering::RenderedObject<
                std::vector<rendering::ColoredRectangle>> *>(o));
        break;

      case rendering::types::Text:
        this->renderer->renderObject<rendering::Text>(
            *dynamic_cast<rendering::RenderedObject<rendering::Text> *>(o));
        break;

      // Unknown entity
      default:
        std::cerr << "Unknown type : " << o->type << std::endl;
        exit(EXIT_FAILURE);
        break;
      }
  }
  this->renderer->updateWindow();
  //  SDL_UpdateWindowSurface(this->ctx->getWindow());
}

void MainLoop::physics() {
  for (physics::PhysicsObject *p : this->physics_entities) {
    if (p)
      p->poll();
  }
}

void MainLoop::update() {
  auto copy_vector =
      std::vector<rendering::GeneralRenderedObject *>(this->rendered_entities);
  for (rendering::GeneralRenderedObject *o : copy_vector) {
    if (o)
      if (!o->updateDisabled())
        o->update(*this->ctx);
  }
}

void MainLoop::delete_loop() {
  for (size_t i = 0; i < this->rendered_entities.size(); ++i) {
    if (this->rendered_entities[i])
      if (this->rendered_entities[i]->toBeDeleted()) {
        delete this->rendered_entities[i];
        // We make it a nullptr so we don't have to move memory around too much
        this->rendered_entities[i] = nullptr;
      }
  }
  for (size_t i = 0; i < this->physics_entities.size(); ++i) {
    if (this->physics_entities[i])
      if (this->physics_entities[i]->toBeDeleted()) {
        // Don't delete the object if it is a rendered object as well to
        // avoid freeing it twice
        if (!this->physics_entities[i]->isRendered())
          delete this->physics_entities[i];
        this->physics_entities[i] = nullptr;
      }
  }
}

void MainLoop::stop() {
  this->quit = true;
}

void MainLoop::handleInputs(const Uint8 *keys) {
  for (rendering::GeneralRenderedObject *o : this->rendered_entities) {
    if (o && !o->keyDisabled())
      o->onKeyPressed(keys, *this->ctx);
  }
}

} // namespace engine