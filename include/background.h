#pragma once

#include "rendered_object.h"

namespace rendering {

// Represents the background texture (explicit)
class Background : public RenderedObject<SDL_Rect> {
  private:
    // Size of the background sprite
    // IMPORTANT: Differ from the w/h of the sprite_pos, works like a step 
    // of iteration of the background sprite (as a tile)
    int size_x, size_y;
    
  public:
    // Constructor
    Background(SDL_Rect sprite_pos, int size_x, int size_y);

    void init(Context& ctx) override {};
    void update(Context& ctx) override {};
    void onKeyPressed(const Uint8 *keys, Context &ctx) override {};

    // Return the visual object of the background sprite
    SDL_Rect render() override;

    // Getters/Setters
    inline int getSizeX() { return this->size_x; }
    inline int getSizeY() { return this->size_y; }
};

}