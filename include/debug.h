#include <iostream>
#include <string>

namespace debug {

// Debug prints
class Debug {
public:
  inline static void print(std::string msg) {
    #ifdef MODE_DEBUG
    std::cerr << msg << std::endl;
    #endif
  }
};

}