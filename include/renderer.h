#pragma once

#include <SDL.h>
#include <memory>
#include <iostream>
#include <vector>

#include "rendered_object.h"
#include "background.h"

namespace rendering
{

  class Renderer
  {
  private:
    // Explicit
    std::unique_ptr<Background> background = nullptr;

    // SDL basic structures for rendering
    SDL_Texture *sprite_atlas;
    SDL_Window *window;
    SDL_Renderer *sdl_rend;

    void renderColoredRectangle(ColoredRectangle cr);

  public:
    // Constructor: define the SDL structures
    Renderer(SDL_Surface *sprite_atlas, const char *name, int width, int height);
    ~Renderer();
    
    // Background explicit
    void setBackground(Background &&background);

    // Tile background from the homemade structure
    void renderBackground();

    template <typename T>

    // Render a specific visual object
    void renderObject(RenderedObject<T> &object_to_render);

    // SDL rendering round
    void updateWindow();

    std::pair<int, int> getWindowSize();

    inline SDL_Window *getWindow() { return this->window; }

    inline void setWindowResizable(bool b) {
      SDL_SetWindowResizable(this->window, (SDL_bool) b);
      };
      
    void clearWindow(int width, int height);
  };

} // namespace rendering