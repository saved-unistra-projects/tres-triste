#include <getopt.h>
#include <cstdlib>

struct help {
  const char *option;
  const char *help;
};

struct help option_helps[] = {
  {"-p, --players", "possible values are 1 or 2, chooses the number of players. default is 1."},
  {"-b, --bot"},
  {NULL, NULL}
};

struct option long_options[] = {
  {"players", required_argument, 0, 'p'},
  {"bot", no_argument, 0, 'b'},
  {NULL, 0, NULL, 0}
};