#pragma once

#include <iostream>

#include "rendered_object.h"
#include "colored_rectangle.h"
#include "collision_object.h"
#include "play_area.h"

#define BLOCKS_WIDE 10
#define BLOCKS_HIGH 20

#define DEPTH_MULT 0.75
#define LIGHT_COLOR_DIFF 50

namespace game {
  class TetrisBlock : public rendering::RenderedObject<std::vector<rendering::ColoredRectangle>>, public physics::CollisionObject  {
    private:
      // Offset from the grid
      static const int offset = 200;
      // Color of the block
      types::Color color;
      
      // Play area (used for rendering calculations)
      game::PlayArea &pa;
      
    public:
      // Dimensions of the block
      static const int width = 25, height = 25;
      
      size_t getType() { return typeid(TetrisBlock).hash_code(); }
      TetrisBlock(int x, int y, game::PlayArea &pa, types::Color color = types::Color(255, 255, 255), int layer = 0);
      void update(rendering::Context &ctx) {};
      void init(rendering::Context &ctx) {};
      void onKeyPressed(const Uint8 *keys, rendering::Context &ctx) {};
      std::vector<rendering::ColoredRectangle> render();
      
      void incrementY();
      void incrementX();
      void decrementX();
      inline void setX(int x) { this->x = x; };
      inline void setY(int y) { this->y = y; };
      inline int getX() { return this->x; };
      inline int getY() { return this->y; };
  };
}