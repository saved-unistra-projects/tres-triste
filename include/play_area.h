#pragma once

#include "rendered_object.h"

namespace game {
class PlayArea : public rendering::RenderedObject<rendering::ColoredRectangle> {
public:
  enum Anchor { Left, Right };

private:
  int multiple_x, multiple_y;
  types::Color color;
  Anchor anchor = Left;

  /* Optionnal definition of a position. Otherwise, the play area
     is centered. */
  bool pos_defined = false;
  std::pair<int, int> pos;
  bool size_defined = false;
  std::pair<int, int> size;

public:
  PlayArea(int multiple_x, int multiple_y,
           types::Color color = types::Color{0, 0, 0}, Anchor anchor = Left)
      : color{color}, multiple_x{multiple_x}, multiple_y{multiple_y} {
    this->no_update = true;
  };

  virtual void update(rendering::Context &ctx) override{};
  void onKeyPressed(const Uint8 *keys, rendering::Context &ctx) override{};
  rendering::ColoredRectangle render();

  std::pair<int, int> computePos();
  std::pair<int, int> computeSize();

  inline void setAnchor(Anchor a) { this->anchor = a; };

  inline void setPos(int x, int y) {
    this->pos = std::make_pair(x, y);
    this->pos_defined = true;
  };
  
  inline void setSize(int width, int height) {
    this->size = std::make_pair(width, height);
    this->size_defined = true;
  }
};

class DoublePlayArea : public rendering::GeneralRenderedObject {
  private:
    PlayArea *pa1, *pa2;
  public:
    DoublePlayArea(PlayArea *pa1, PlayArea *pa2) : pa1{pa1}, pa2{pa2} {};
    void init (rendering::Context &ctx);
    void update (rendering::Context &ctx);
    void onKeyPressed(const Uint8 *keys, rendering::Context &ctx) override {};
    
    inline PlayArea *getPa1() { return this->pa1; };
    
    inline PlayArea *getPa2() { return this->pa2; };
    
};
} // namespace game