#pragma once

#include <cstdlib>
#include <vector>

#include "color.h"

#define WIDEST_WIDTH 4
#define HEIGHEST_HEIGHT 3


namespace game {
class TetrisShape {
  using Shape = std::vector<std::pair<int, int>>;
  private:
    size_t weight = 1;
    Shape shape;
    types::Color color;
    bool rotation = true;
    bool limited_rotation = false;
    
  public:
    /* Parse shape from file. File format must be the following :

       <weight : int>
       <red : int> <green : int> <blue : int>
       <number_of_blocks : int>
       (number_of_blocks lines with the x and y coordinates
        relative to the center separated by a space, (the center
        is coordinates (0, 0)))
    */
    TetrisShape(const char *filename);
    TetrisShape(Shape shape, size_t weight = 1, types::Color = types::Color(0, 0, 0));
    inline size_t getWeight() { return this->weight; };
    inline Shape getShape() { return this->shape; };
    inline types::Color getColor() { return this->color; };
    inline bool getRotation() { return this->rotation; };
    inline bool getLimitedRotation() { return this->limited_rotation; };
};
}