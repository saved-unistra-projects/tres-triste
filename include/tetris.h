#pragma once

#include <random>

#include "tetris_piece.h"
#include "tetris_image.h"
#include "rendered_object.h"
#include "score_label.h"
#include "globals.h"
#include "rendered_object.h"
#include "tetris_piece.h"
#include "label.h"

namespace game
{
  // Change this, semantically, Tetris should be any object
  class Tetris : public rendering::GeneralRenderedObject
  {
    using TetrisShapePtr = std::shared_ptr<TetrisShape>;
    private:
      // TODO Change this to vector of shared_ptr
      std::vector<TetrisShapePtr> piece_pool;
      std::vector<TetrisBlock *> blocks;
      TetrisPiece *current_piece = nullptr;
      TetrisImage *next_piece = nullptr;
      // Current shape of the piece
      TetrisShapePtr current_shape = nullptr;
      size_t weight_sum;
      int size_x, size_y;

    private:
      TetrisPiece &parsePieceFile(const char *filename);
      /* Basic initialization method, used in both constructors */
      void init();
      TetrisShapePtr selectRandomPiece();
      size_t computeWeightSum();
      void addBlocks(TetrisPiece *);
      void eliminateLines();
      PlayArea *pa;
      int speed = 1;
      ui::ScoreLabel *score;
      ui::Side side = ui::Left;
      Label *preview_label;
      
      // Time elapsed since last input (when ai controlled)
      double bot_time = 0.;
      double bot_threshold = .3;
      // Key to press next (ai controlled)
      int key_to_press = -1;
      
      // Game is ended
      bool game_ended = false;
      
      // Collision layer
      int layer = 0;

      // Seeded RNG for random pieces
      std::mt19937 generator = std::mt19937();

      // Commands
      TetrisPiece::PieceControls piece_controls = TetrisPiece::PieceControls();
      
      // Button used for the hold
      Uint8 hold_key;
      // Was the hold already used ?
      bool used_hold = false;
      // Actual piece held
      TetrisShapePtr held_piece = nullptr;
      TetrisImage* held_image = nullptr;
      Label *held_label;
      
      // Is this tetris game controlled by ai ?
      bool ai_controlled = false;
    
  public:
    Tetris();
    Tetris(std::vector<std::shared_ptr<TetrisShape>> piece_pool, PlayArea *pa,
           int size_x = 10, int size_y = 20);
    // Parse pieces from a directory
    Tetris(const char *directory, PlayArea *pa, int size_x = 10, int size_y = 20);
    ~Tetris();
    void init(rendering::Context &ctx);
    void initTetris();
    void update(rendering::Context &ctx) override;
    void onKeyPressed(const Uint8 *keys, rendering::Context &ctx) override;
    inline PlayArea *getPlayArea() { return this->pa; };
    inline void setLayer(int layer) { this->layer = layer; };
    inline void setPieceControls(TetrisPiece::PieceControls pc) {
      this->piece_controls = pc;
      this->hold_key = pc.reserve;
    };
    inline int getScore() { return this->score->getScore(); };
    inline void setSeed(int seed) { this->generator = std::mt19937(seed); };
    inline void setInterfaceSide(ui::Side side) { this->side = side; };
    inline void setAIControlled(bool b) { this->ai_controlled = b; };
  };
} // namespace game