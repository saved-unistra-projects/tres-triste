#pragma once

// Base for all objects (litteraly)
class GeneralObject {
  private:
    bool to_be_deleted = false;
  public:
    virtual size_t getType() = 0;
    inline void markDelete() { this->to_be_deleted = true; };
    inline bool toBeDeleted() { return this->to_be_deleted; };
    
};