#pragma once

#include <SDL.h>

namespace types {

// Basic color, rgb tmtc
class Color {
public:
  Uint8 r, g, b;
  
public:
  Color(Uint8 r, Uint8 g, Uint8 b);
  Color();
};

}