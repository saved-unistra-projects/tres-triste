#pragma once

#include <SDL.h>

#include "color.h"

namespace rendering {
  class ColoredRectangle {
    private:
      int x, y, width, height;
      ::types::Color color;

    public:
      // Constructor: default mode = 1px white square at the origin
      ColoredRectangle(int x = 0, int y = 0, int width = 1, int height = 1, 
        ::types::Color color = ::types::Color{255, 255, 255});
      
      // Convert into SDL structure
      SDL_Rect toSDL_Rect();

      // Getters/Setters
      ::types::Color getColor();
  };
}