#pragma once 

#include <iostream>

#include "rendered_object.h"
#include "play_area.h"
#include "text.h"

namespace game {
  class Label : public rendering::RenderedObject<rendering::Text> {
    protected:
      int x, y, width, height, size;
      int original_x, original_y;
      std::string text, font_name;
      ui::Side side = ui::Right;

      types::Color color;
      int style;
      game::PlayArea *pa;

    public:
      Label(int x, int y, int width, int height, std::string text, std::string font_name, int size, types::Color color = types::Color(255, 255, 255), int style = NORMAL);
      void update(rendering::Context &ctx);
      void init(rendering::Context &ctx) {};
      void onKeyPressed(const Uint8 *keys, rendering::Context &ctx) {};
      rendering::Text render();

      // Place the texture right or left from the play-area
      void setPlayArea(game::PlayArea* pa, ui::Side side);

  };
}