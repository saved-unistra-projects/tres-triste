#pragma once

#include <vector>
#include <SDL.h>

#include "color.h"
#include "tetris_block.h"
#include "colored_rectangle.h"
#include "rendered_object.h"
#include "play_area.h"
#include "tetris_shape.h"
#include "ui.h"


namespace game
{
  class TetrisImage : public rendering::RenderedObject<std::vector<rendering::ColoredRectangle>>
  {
    // TODO: Make this better
    using TetrisShapePtr = std::shared_ptr<TetrisShape>;

  private:
    // List of blocks that compose the piece
    std::vector<TetrisBlock *> blocks;
    
    // Initial coordinates
    int x, y;
    int width, height;
    
    // Shape of the piece
    TetrisShapePtr shape;
    
    // Play Area
    game::PlayArea &pa;


  public:
    TetrisImage(int x, int y, TetrisShapePtr shape, game::PlayArea &pa, ui::Side side);
    void init(rendering::Context &ctx)  {};
    void update(rendering::Context &ctx) {};
    void onKeyPressed(const Uint8 *keys, rendering::Context &ctx) {};
    std::vector<rendering::ColoredRectangle> render();
    
    void registerBlocks();
    inline TetrisShapePtr getShape() { return this->shape; };
  };
} // namespace game