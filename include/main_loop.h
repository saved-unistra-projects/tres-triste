#pragma once

#include <SDL.h>
#include <SDL_ttf.h>
#include <vector>
#include <functional>
#include <typeinfo>
#include <iostream>

#include "context.h"
#include "renderer.h"
#include "rendered_object.h"
#include "background.h"
#include "collision_object.h"

using callback_func = std::function<void(void)>&;

namespace engine {
// Represents the game loop (explicit)
class MainLoop {
private:
  // Game window and renderer
  rendering::Context *ctx;
  rendering::Renderer *renderer;

  // Entities to render
  std::vector<rendering::GeneralRenderedObject*> rendered_entities;

  // Entities to compute physics on
  std::vector<physics::PhysicsObject*> physics_entities;

  // Unused
  std::vector<std::tuple<SDL_Scancode, callback_func>> callbacks;

  // Last and current performance counter (framerate)
  Uint64 prev, now;

  // Upper frames per second limit
  float fps_cap = 60.f;
  
  // Loop has started ?
  bool has_started = false;
  
  // Does the loop need to end ?
  bool quit = false;

public:
  // Constructor/Destructor 
  MainLoop();
  ~MainLoop();

  // Render the background of the window
  void setBackground(rendering::Background&& background);

  // Initialize the second/frame counter and call init/loop
  void start();

  // Classical loop structure, handles event, delta computation, rendering
  void loop();
  
  // Call init functions of registered objects
  void init();

  // Add an object to the rendering/physics vectors
  void registerObject(GeneralObject *obj);
  
  // Get the renderer (usefull to get window size for example)
  inline rendering::Renderer &getRenderer() { return *this->renderer; };
  
  // End the loop
  void stop();
  
private:
  // Render the background and every specified entity
  void render();
  
  // Run physics step
  void physics();

  // Update the visual objects displayed
  void update();
  
  // Delete objects
  void delete_loop();
  
  void handleInputs(const Uint8 *keys);
};
}