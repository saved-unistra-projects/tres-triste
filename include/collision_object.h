#pragma once

#include <memory>
#include <vector>

#include "general_object.h"

namespace physics {
  class PhysicsObject : virtual public GeneralObject {
    protected:
      // Is the object a rendered object as well ?
      bool is_rendered = false;
      
    public:
      virtual void poll() = 0;
      size_t getType() { return typeid(PhysicsObject).hash_code(); }
      inline void setIsRendered(bool b) { this->is_rendered = b; };
      inline bool isRendered() { return this->is_rendered; };
  };
  class CollisionObject : public PhysicsObject{
    public:
      enum Direction {
      Up = 1,
      Down = 2,
      Left = 4,
      Right = 8,
      On = 16
    };
    private:
      // All the collision objects
      static std::shared_ptr<std::vector<CollisionObject *>> collision_grid;
      // Bitwise direction where the object collides
      int collision_mask = 0;
      // Borders of the grid
      static int min_x, min_y, max_x, max_y;
      static int last_id;
      int id;
      int position_in_grid;
      bool is_ghost = false;
      int layer; 
    
    protected:
      int x, y;
    public:
      CollisionObject(int x, int y, int layer = 0);
      ~CollisionObject();
      // Updates the collision mask
      void poll();
      int getCollisionMask();
      int getId();
      void setGhost(bool is_ghost);
      bool isGhost();
      static void setBorders(int min_x, int min_y, int max_x, int max_y);
      // Unregister this object from the grid
      void unregister();
      // Get layer
      inline int getLayer() { return this->layer; };
      
    private:
      void checkBorders();
};
}