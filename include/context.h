#pragma once

#include <SDL.h>
#include <memory>

namespace rendering {

// Represents the game window
class Context {
  private:
    /* Can't actually store a smart pointer because SDL_Window is an incomplete
    * type.*/
    SDL_Window *window; // Explicit

    // Time elapsed per frame (Second/Frame during the last loop)
    double delta;

  public:
    // Constructor: define the SDL_Window
    Context(SDL_Window *window);

    // Getters/Setters
    int getWidth();   // => from SDL_Window
    int getHeight();  // => from SDL_Window
    inline double getDelta() { return this->delta; }
    inline SDL_Window *getWindow() { return this->window; }
    inline void setDelta(double delta) { this->delta = delta; }
};

} // namespace rendering