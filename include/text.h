#pragma once

#include <SDL.h>
#include <SDL_ttf.h>
#include <string>

#include "color.h"
#include "ui.h"

#define FONT_REP "fonts/"

#define BOLD TTF_STYLE_BOLD
#define ITALIC TTF_STYLE_ITALIC
#define UNDERLINE TTF_STYLE_UNDERLINE
#define NORMAL TTF_STYLE_NORMAL

// enum Column {
//   LEFT, MIDDLE, RIGHT
// };

namespace rendering {

class Text {
  private:
    // General properties
    int x, y, width, height;
    ::types::Color color;

    // Text properties
    std::string text;
    std::string font_name;
    int size;
    int style;
  
  public:
    Text(int x = 0, int y = 0, int width = 50, int height = 50, std::string text = "default", std::string font_name = "Ubuntu-C.ttf", int size = 25, 
      ::types::Color color = ::types::Color{255, 255, 255}, int style = NORMAL);
    
    SDL_Rect toSDL_Rect();
    SDL_Texture* getTexture(SDL_Renderer *renderer);

    // Getters/Setters
    ::types::Color getColor();
    const char* getText();
    const char* getFontName();
    int getFontSize();
    int getStyle();
};

}

