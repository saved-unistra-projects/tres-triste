#pragma once

#include <SDL.h>
#include <typeinfo>

#include "colored_rectangle.h"
#include "context.h"
#include "text.h"
#include "general_object.h"

enum Side {
  LEFT, RIGHT
};

namespace rendering {

namespace types {
// Type des différents objets de rendu: SDL_Rect de manière générale,
// Colored_Rectangle pour un rectangle de couleur
enum RenderedObjectType {
  GeneralRenderedObject,
  SDL_Rect,
  ColoredRectangle,
  ColoredRectangleList,
  Text
};
} // namespace types

// Base for all visual representation of objects
class GeneralRenderedObject : virtual public GeneralObject {
public:
  size_t type;

protected:
  bool no_update = false;
  bool no_key_press = false;

public:
  // To differenciate the different RenderedObjects (due to the templates)
  size_t getType() { return typeid(GeneralRenderedObject).hash_code(); }

  // Set the type when created
  GeneralRenderedObject(types::RenderedObjectType t) { this->type = t; };

  // Default constructor
  GeneralRenderedObject() { this->type = types::GeneralRenderedObject; };

  // Inheritance-reserved
  virtual void init(Context &ctx){};
  virtual void update(Context &ctx) = 0;
  virtual void onKeyPressed(const Uint8 *keys, Context &ctx) = 0;
  inline bool updateDisabled() { return this->no_update; };
  inline bool keyDisabled() { return this->no_key_press; };
};

// Visual objects
template <typename T> class RenderedObject : public GeneralRenderedObject {
protected:
  // Position of the sprite on the sprite sheet
  SDL_Rect sprite_pos = {0, 0, 0, 0};

public:
  RenderedObject();

  // Getters/Setters
  inline SDL_Rect getSpritePos() { return this->sprite_pos; };

  // Inheritance-reserved
  virtual T render() = 0;
};

} // namespace rendering