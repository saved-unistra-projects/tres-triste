#pragma once

#include <vector>
#include <SDL.h>

#include "color.h"
#include "tetris_block.h"
#include "rendered_object.h"
#include "play_area.h"
#include "tetris_shape.h"

#define FAST_SPEED 0.05

namespace game
{
  class TetrisPiece : public rendering::RenderedObject<SDL_Rect>
  {
    // TODO: Make this better
    using TetrisShapePtr = std::shared_ptr<TetrisShape>;
    enum Move {
      Down,
      Left,
      Right,
      Rotate,
    };
    
    enum RotationDirection {
      AntiClockwise,
      Clockwise,
    };
    
    public:
    class PieceControls {
      public:
        Uint8 down, left, right, rotate, reserve;
        PieceControls(Uint8 down = SDL_SCANCODE_DOWN,
                      Uint8 left = SDL_SCANCODE_LEFT,
                      Uint8 right = SDL_SCANCODE_RIGHT,
                      Uint8 rotate = SDL_SCANCODE_UP,
                      Uint8 reserve = SDL_SCANCODE_RETURN) :
                      down{down}, left{left}, right{right},
                      rotate{rotate}, reserve{reserve} {};
    };

  private:
    // List of blocks that compose the piece
    std::vector<TetrisBlock *> blocks;
    
    // Initial coordinates
    int x, y;
    
    // Timer for speed calculation
    double time_passed = 0.;
    double time_until_drop = 1.;
    // Needed to remember old speed while accelerating
    double old_time_until_drop;
    

    bool accelerating = false; 
    
    // Global collision mask of all the blocks
    int collision_mask = 0;
    bool is_alive = true;
    
    // Shape of the piece
    TetrisShapePtr shape;
    
    // Timers used for key repetition
    const double key_repeat_timer = 0.1;
    double key_timer_counter = 0.;
    // Used to avoid spam
    int key_to_watch = -1;
    // Must use a different variable for rotation
    bool rotation_used = false;
    double rotation_counter = 0.;
    
    // Can this piece rotate ?
    bool rotation = true; 
    
    // Should this piece only be rotated anti-clockwise, then clockwise ?
    bool limited_rotation = false;
    
    // If limited rotation is used, this is used to indicate if the piece
    // should rotate the other way next
    bool rotated = false;
    
    // Keys to control the piece
    PieceControls pc;
    
    // Play Area
    game::PlayArea &pa;
    
    /* True of the piece has moved for this frame (only one movement allowed 
    per frame) */
    bool moved_this_frame = false;
    
    // Collision layer
    int layer = 0;
    
 public:
    /* Build a new tetris piece from its position, shape (a vector of
  pairs, representing coordinates, such that (0, 0) is the top left corner),
  and its color. */
    TetrisPiece(int x, int y, TetrisShapePtr shape, game::PlayArea &pa, int speed = 1, int layer = 0, PieceControls pc = PieceControls());
    void destruct();
    void rotate();
    void init(rendering::Context &ctx) override {};
    void update(rendering::Context &ctx) override;
    void onKeyPressed(const Uint8 *keys, rendering::Context &ctx) override;
    SDL_Rect render() { return SDL_Rect{}; };
    inline bool isAlive() { return this->is_alive; };
    void registerBlocks();
    inline std::vector<TetrisBlock *> &getBlocks() { return this->blocks; };
    void destroyBlocks();
    inline int getCollisionMask() { return this->collision_mask; };
    void pressKey(Uint8 scancode, rendering::Context &ctx);
    inline void disableKeyPress() { this->no_key_press = true; };

  private:
    void move(Move m);
    void accelerate(bool b);
    void checkCollisions();
    void rotate(RotationDirection rd);
    // Correct starting position for piece where the center isn't at the top.
    void correctStartingPos();
  };
} // namespace game