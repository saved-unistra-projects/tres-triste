#pragma once

#include <iostream>
#include <string>

#include "rendered_object.h"
#include "label.h"
#include "ui.h"

namespace ui {
  class ScoreLabel : public game::Label {
    using game::Label::Label;
    protected:
      unsigned int score = 0;

    public:
      // Side-effect: Change the text
      void addToScore(unsigned int points);
      void setScore(unsigned int score);
      inline unsigned int getScore() { return this->score; };
  };
}