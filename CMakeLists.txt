cmake_minimum_required(VERSION 3.7)

# Set project name
set(TARGET "TresTriste")
project(${TARGET} LANGUAGES CXX)

# include directories
include_directories(include)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_MODULE_PATH cmake_modules)

find_package(SDL2 REQUIRED)
find_package(SDL2TTF REQUIRED)

if (DEFINED ENV{MODE_DEBUG})
  add_compile_options(-D MODE_DEBUG)
  add_compile_options(-g)
endif()

add_executable(${TARGET} "")
aux_source_directory(src SOURCES)
aux_source_directory(include SOURCES)
target_sources(${TARGET} PUBLIC ${SOURCES})
if (${CMAKE_SYSTEM_NAME} MATCHES Linux)
  target_include_directories(${TARGET} PUBLIC /usr/include/SDL2)
  target_link_libraries(${TARGET} PUBLIC SDL2 SDL2main ${SDL2TTF_LIBRARY})
else()
  target_link_libraries(${TARGET} PUBLIC SDL2::SDL2 SDL2::SDL2main SDL2::TTF)
endif()
target_compile_definitions(${TARGET} PUBLIC _USE_MATH_DEFINES)
