# Très-triste

## Installation

À la racine du projet:

1. `cmake .`
2. `make -j8`
3. `./TresTriste` et les options éventuelles désirées (`-p 2` pour 2 joueurs, suivi de `-b` si l'on souhaite que le second joueur soit un bot)

**Important**:
- La libraire SDL_ttf doit être installée au préalable (`apt-get libsdl2-ttf-dev`)- En raison du chemin des fichiers de pièces, le projet doit impérativement être lancé depuis sa racine.
